import tensorflow as tf
import pandas as pd
import cv2

def get_config(per_process_gpu_memory_fraction=0.8, allow_growth=True):
    config = tf.compat.v1.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = per_process_gpu_memory_fraction
    config.gpu_options.allow_growth=True
    print("== Configuring GPU ==".center(20))
    print("GPU Memory Fraction (per process): {}".format(per_process_gpu_memory_fraction))
    print("Allow Growth option is set to {}".format(allow_growth))
    print("Check config.py file for changing the parameters")
    print("====".center(20))
    return config


def get_label_mapping_dict(path_to_label_csv):
    df = pd.read_csv(path_to_label_csv, names=['label', 'id'])
    return dict(zip(df.to_dict('list')['id'], df.to_dict('list')['label']))


def text_over_rectangle(coords, text, image, color):
    x, y = coords
    
    # font 
    fontFace = cv2.FONT_HERSHEY_SIMPLEX 
    
    # fontScale 
    fontScale = 1
    # Line thickness of 2 px 
    thickness = 3
    # Using cv2.putText() method 
    
    fontColor = (255, 255, 255)
    
    (text_width, text_height) = cv2.getTextSize(text.capitalize(), fontFace, fontScale, thickness)[0]
    bg_coords = ((int(x), int(y)-text_height-30), (int(x)+text_width+10, int(y)))
    # cv2.rectangle(image, bg_coords[0], bg_coords[1], color, cv2.FILLED)
    cv2.putText(image, text.capitalize(), (int(x), int(y+20)), fontFace, fontScale, fontColor, thickness)