#!/usr/bin/env python
# coding: utf-8

from utils import imports
from utils import helpers
from utils import config

import tensorflow as tf
import numpy as np

class tensorflow_object_detection_api_1(object):

	def __init__(self):
		self.__path_to_frozen_graph = config.PATH_TO_FROZEN_GRAPH
		self.__detection_graph = self.__load_detection_graph()
		self.__input_tensor, self.__output_tensor_dict = self.__get_input_output_tensors()
		self.__config = helpers.get_config()
		self.__session = tf.compat.v1.Session(graph=self.__detection_graph, config=self.__config)
		
	def __load_detection_graph(self):
		# A TensorFlow computation, represented as a dataflow graph.
		# Using graphs directly is now deprecated - TensorFlow 2 onwards
		detection_graph = tf.Graph()

		# A default graph can be registered with the tf.Graph.as_default context manager. 
		# Then, operations will be added to the graph instead of being executed eagerly. For example:
		with detection_graph.as_default():
			# now whatever will happen below will happen in this scope only
			# this scope is for the graph we instantiated above and not the default graph which tensorflow provides
			# check: https://www.tensorflow.org/api_docs/python/tf/compat/v1/get_default_graph

			# tf.GraphDef(): A ProtocolMessage
			# https://github.com/tensorflow/tensorflow/blob/v2.5.0/tensorflow/core/framework/graph.proto

			od_graph_def = tf.compat.v1.GraphDef()

			with tf.io.gfile.GFile(self.__path_to_frozen_graph, 'rb') as fid:
				serialized_graph = fid.read()
				# as the name suggests, it parses the loaded serilzed graph into a sort of container
				od_graph_def.ParseFromString(serialized_graph)
				# once parsed, it gets loaded rather imported to detection_graph that can be used later
				tf.import_graph_def(od_graph_def, name='')
		return detection_graph
	
	def __get_input_output_tensors(self):
		image_tensor = self.__detection_graph.get_tensor_by_name('image_tensor:0')
		
		# get all ops
		ops = self.__detection_graph.get_operations()
		
		# get all tensor names
		all_tensor_names = {output.name for op in ops for output in op.outputs}
		
		tensor_dict = {}
		for key in ['num_detections', 'detection_boxes', 'detection_scores', 'detection_classes', 'detection_masks']:
			tensor_name = key + ':0'
			if tensor_name in all_tensor_names:
				tensor_dict[key] = self.__detection_graph.get_tensor_by_name(tensor_name)
		
		return image_tensor, tensor_dict
	
	def predict(self, image, detection_threshold=0.5, class_mapping=None):
		return self.__session.run(self.__output_tensor_dict, feed_dict={self.__input_tensor: np.expand_dims(image, 0)})