#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Author: Anuj Khandelwal
Date: 29-06-2021
Last Modified: 14-06-2021
"""

import os




INPUT_TYPE = 'image_tensor'
TRAINED_MODEL_PIPELINE_CONFIG_PATH = '/home/ubuntu/raid/computer_vision/aws-s3/dna-computer-vision/COMPUTER_VISION/dataset/open_source/vehicle/iiit-hyderabad/training/PIPELINE_CONFIG/2021_08_16/ssd_inception_v2_coco_2018_01_28/ssd_inception_v2_coco.config'
TRAINED_MODEL_CHECKPOINT_PREFIX = '/home/ubuntu/raid/computer_vision/aws-s3/dna-computer-vision/COMPUTER_VISION/dataset/open_source/vehicle/iiit-hyderabad/training/MODEL_CHECKPOINTS/2021_08_16/ssd_inception_v2_coco_2018_01_28/checkpoints/model.ckpt-200000'
EXPORTED_MODEL_PATH = '/home/ubuntu/raid/computer_vision/aws-s3/dna-computer-vision/COMPUTER_VISION/dataset/open_source/vehicle/iiit-hyderabad/training/MODEL_CHECKPOINTS/2021_08_16/ssd_inception_v2_coco_2018_01_28/export'


cmd = 'python /home/ubuntu/mount/Notebooks/models/research/object_detection/export_inference_graph.py \
--input_type={} \
--pipeline_config_path={} \
--trained_checkpoint_prefix={} \
--output_directory={}'.format(\
                              INPUT_TYPE, \
                              TRAINED_MODEL_PIPELINE_CONFIG_PATH, \
                              TRAINED_MODEL_CHECKPOINT_PREFIX, \
                              EXPORTED_MODEL_PATH)


os.system(cmd)