# Training Pipeline


## References
1. [Roboflow Tensorflow Object Detection API Pipeline](https://github.com/roboflow-ai/tensorflow-object-detection-faster-rcnn)
2. [How to Train Your Own Object Detector Using TensorFlow Object Detection API](https://neptune.ai/blog/how-to-train-your-own-object-detector-using-tensorflow-object-detection-api)

## Other applications
* [Train a Mask R-CNN model with the Tensorflow Object Detection API](https://gilberttanner.com/blog/train-a-mask-r-cnn-model-with-the-tensorflow-object-detection-api)


## Common issues
* Issue: ValueError: First step cannot be zero. https://github.com/tensorflow/models/issues/3794
* Resolved: 
    * https://github.com/EdjeElectronics/TensorFlow-Object-Detection-API-Tutorial-Train-Multiple-Objects-Windows-10/issues/51
    * https://stackoverflow.com/questions/50203766/custom-object-training-tensor-flow-error
* Object Detection Tutorial and Installation.md version conflict #8556:
* https://github.com/tensorflow/models/pull/8556
