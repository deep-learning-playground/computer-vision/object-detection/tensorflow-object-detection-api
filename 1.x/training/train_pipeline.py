#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Author: Anuj Khandelwal
Date: 29-06-2021
Last Modified: 14-06-2021
"""


import argparse
from argparse import ArgumentParser, SUPPRESS
from pathlib import Path
import sys

from datetime import datetime
from pathlib import Path
import re
import os
import tensorflow as tf

# os.sys.path.append("/home/ubuntu/mount/Notebooks/models/research/")
# os.sys.path.append("/home/ubuntu/mount/Notebooks/models/research/slim/")


# use sys path append here instead
# from object_detection.utils import label_map_util
# from google.protobuf import text_format
# from object_detection.protos import pipeline_pb2


import json

from platform import python_version
print("Python Version: ", python_version())
 
def build_argparser():
    parser = ArgumentParser(add_help=False)
    args = parser.add_argument_group('Options')
    args.add_argument('-h', '--help', action='help', default=SUPPRESS, help='Show this help message and exit.')
    args.add_argument('-c', '--config', help='Required. Path to a config.json file for model to be trained.',
                      required=True, type=Path)

    args.add_argument('-m', '--models', help='Path to tensorflow models', required=True, type=Path)
    return parser

def read_config(path_to_config):
    with open(path_to_config) as f:
        config = json.load(f)
    return config

def get_num_classes(pbtxt_fname):
    label_map = label_map_util.load_labelmap(pbtxt_fname)
    categories = label_map_util.convert_label_map_to_categories(
        label_map, max_num_classes=90, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)
    return len(category_index.keys())

def makedir(abs_dir_path):
    Path(abs_dir_path).mkdir(parents=True, exist_ok=True)    

def assert_file(abs_file_path):
    assert os.path.isfile(abs_file_path), '`{}` does not exist.'.format(abs_file_path)


def assert_dir(abs_dir_path):
    assert os.path.isdir(abs_dir_path), '`{}` does not exist.'.format(abs_dir_path)


def check_files(config_json):
    assert_file(config_json['TRAIN_TFRECORD_FILE_PATH'])
    assert_file(config_json['VALIDATION_TFRECORD_FILE_PATH'])
    assert_file(config_json['LABEL_MAP_PBTXT_FILE_PATH'])
    assert_file(config_json['TF_PIPELINE_CONFIG_FILE_PATH'])
    
    assert_dir(config_json['USER_MODEL_PIPELINE_CONFIG_DIR_PATH'])
    assert_dir(config_json['USER_MODEL_CHECKPOINTS_DIR_PATH'])
    assert_dir(config_json['TF_PRETRAINED_MODEL_DIR_PATH'])


def build_user_pipeline_config(config_json, ABS_USER_MODEL_PIPELINE_CONFIG_FILE_PATH):
    
    fine_tune_checkpoint = os.path.join(config_json['TF_PRETRAINED_MODEL_DIR_PATH'], "model.ckpt")
    num_classes = get_num_classes(config_json['LABEL_MAP_PBTXT_FILE_PATH'])
    
    pipeline = pipeline_pb2.TrainEvalPipelineConfig()                                                                                                                                                                                                      

    with tf.io.gfile.GFile(config_json['TF_PIPELINE_CONFIG_FILE_PATH'], "r") as f:                                                                                                                                                                                                                     
        proto_str = f.read()                                                                                                                                                                                                                                          
        text_format.Merge(proto_str, pipeline)

    pipeline.train_config.fine_tune_checkpoint = fine_tune_checkpoint
    pipeline.train_input_reader.label_map_path = config_json['LABEL_MAP_PBTXT_FILE_PATH']
    pipeline.train_input_reader.tf_record_input_reader.input_path[0] = config_json['TRAIN_TFRECORD_FILE_PATH']

    pipeline.eval_input_reader[0].label_map_path = config_json['LABEL_MAP_PBTXT_FILE_PATH']
    pipeline.eval_input_reader[0].tf_record_input_reader.input_path[0] = config_json['VALIDATION_TFRECORD_FILE_PATH']
    
    pipeline.train_config.batch_size = config_json['hyperparams']['BATCH_SIZE']
    pipeline.train_config.num_steps = config_json['hyperparams']['NUM_TRAIN_STEPS']
    
    config_text = text_format.MessageToString(pipeline)                                                                                                                                                                                                        
    with tf.gfile.Open(ABS_USER_MODEL_PIPELINE_CONFIG_FILE_PATH, "wb") as f:                                                                                                                                                                                                                       
        f.write(config_text)
        
    with open(ABS_USER_MODEL_PIPELINE_CONFIG_FILE_PATH) as f:
        s = f.read()
    
    with open(ABS_USER_MODEL_PIPELINE_CONFIG_FILE_PATH, 'w') as f:
        # Set number of classes num_classes.
        s = re.sub('num_classes: [0-9]+',
                   'num_classes: {}'.format(num_classes), s)
        f.write(s)
        
def export_models_path(research_path, slim_path):
    print("==== Exporting models path ====")

    cmd = 'export PYTHONPATH=$PYTHONPATH:{}:{}'.format(research_path, slim_path)
    
    os.system(cmd)
    print()
    
def main(args):
    # args = build_argparser().parse_args()
    tf_model_path = args.models
    research_path = os.path.join(tf_model_path, 'research')
    slim_path = os.path.join(research_path, 'slim')

    print(args.config)
    config_json = read_config(args.config)
    check_files(config_json)
    export_models_path(research_path, slim_path)

    # make a directory with current date - the date on which the model is being trained
    ABS_USER_MODEL_PIPELINE_CONFIG_DIR_PATH = os.path.join(config_json['USER_MODEL_PIPELINE_CONFIG_DIR_PATH'], datetime.now().strftime("%Y_%m_%d"), os.path.split(config_json['TF_PRETRAINED_MODEL_DIR_PATH'])[1])
    makedir(ABS_USER_MODEL_PIPELINE_CONFIG_DIR_PATH)
    assert_dir(ABS_USER_MODEL_PIPELINE_CONFIG_DIR_PATH)

    ABS_USER_MODEL_PIPELINE_CONFIG_FILE_PATH = os.path.join(ABS_USER_MODEL_PIPELINE_CONFIG_DIR_PATH, os.path.split(config_json['TF_PIPELINE_CONFIG_FILE_PATH'])[1])

    # TRAINED_MODEL_DIR
    ABS_TRAINED_MODEL_DIR = os.path.join(config_json['USER_MODEL_CHECKPOINTS_DIR_PATH'], datetime.now().strftime("%Y_%m_%d"), os.path.split(config_json['TF_PRETRAINED_MODEL_DIR_PATH'])[1], 'checkpoints')
    makedir(ABS_TRAINED_MODEL_DIR)
    assert_dir(ABS_TRAINED_MODEL_DIR)


    build_user_pipeline_config(config_json, ABS_USER_MODEL_PIPELINE_CONFIG_FILE_PATH)
    assert_file(ABS_USER_MODEL_PIPELINE_CONFIG_FILE_PATH)

    ## Training
    
    cmd_str = 'python {}\
    --alsologtostderr \
    --model_dir={} \
    --pipeline_config_path={} \
    --num_train_steps={}  \
    --sample_1_of_n_eval_examples={}'.format(\
        os.path.join(research_path, 'object_detection/model_main_custom_gpu.py'), \
        ABS_TRAINED_MODEL_DIR, \
        ABS_USER_MODEL_PIPELINE_CONFIG_FILE_PATH, \
        config_json['hyperparams']['NUM_TRAIN_STEPS'], \
        config_json['hyperparams']['SAMPLE_1_OF_N_EVAL_EXAMPLES']
        )


    os.system(cmd_str)

if __name__ == '__main__':
    args = build_argparser().parse_args()
    
    tf_model_path = args.models
    research_path = os.path.join(tf_model_path, 'research')
    slim_path = os.path.join(research_path, 'slim')

    os.sys.path.append(research_path)
    os.sys.path.append(slim_path)

    # use sys path append here instead
    from object_detection.utils import label_map_util
    from google.protobuf import text_format
    from object_detection.protos import pipeline_pb2

    
    sys.exit(main(args) or 0)
